import React, {useState} from "react";
import {Provider} from "react-redux";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import logo from './logo.svg';
import './App.css';
import FirstPage from "./components/FirstPage";
import {Layout} from "./components/Layout";
import CatAPI from "./pages/CatAPI";
import UnControlledElement from "./pages/Forms/UnControlledElement";
import ContractForm from "./pages/MultiStepsForm";
import Steps from "./pages/Steps";
import InterestingFacts from "./pages/InterestingFacts";
import themes from "./themes";
import ThemesContext from "./Contexts/ThemesContext";
import store from "./state/store";
import { createBrowserHistory } from "history";

const history = createBrowserHistory();

function App() {
  const [themeValue, setThemeValue] = useState(themes.dark);
  return (
    <div className="App">
        <Provider store={store}>
          <ThemesContext.Provider value={{themeValue, setThemeValue}}>
            <Router>
                <Layout/>
                <Routes>
                    <Route exact path="/" element={<CatAPI/>}/>
                    <Route exact path="/contract-form" element={<ContractForm/>}/>
                    <Route exact path="/interesting-facts" element={<InterestingFacts/>}/>
                    <Route exact path="/steps/:id" element={<Steps/>}/>
                </Routes>
            </Router>
          </ThemesContext.Provider>
      </Provider>
    </div>
  );
}

export default App;
