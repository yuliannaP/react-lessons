import React from "react";

import Clock from "./Clock";

class FirstPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isTimerOn: false,
            color: "red"
        }
    }

    timerSwitcher() {
        this.setState({isTimerOn: !this.state.isTimerOn})
    }

    colorSwitcher() {
        this.setState({color: this.state.color === "red"? "blue": "red"})
    }

    render() {
        return (<div>
            <h1>Timer</h1>
            <button onClick={() => this.timerSwitcher()}>Timer switcher</button>
            <button onClick={() => this.colorSwitcher()}>Color switcher</button>
            {this.state.isTimerOn && (<Clock color={this.state.color}/>)}
        </div>);
    }
}

export default FirstPage;
