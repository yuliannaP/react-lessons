import React from "react";
import { Link } from "react-router-dom";


export const Layout = () => {
    return (<div>
        <nav>
            <Link to={"/"}>
                <p>
                    Cat API
                </p>
            </Link>
            <Link to={"/contract-form"}>
                <p>
                    Contract Form
                </p>
            </Link>
            <Link to={"/interesting-facts"}>
                <p>
                    Interesting Facts
                </p>
            </Link>
            <Link to={"steps/77777"}>
                <p>
                    Steps route
                </p>
            </Link>
        </nav>
    </div>);
}
