import React from "react";

class CatAPI extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            catList: [],
            searchValue: "",
            detailCat: null
        }
    }

    showCatList = async () => {
        const allBreeds = 'https://api.thecatapi.com/v1/breeds';
        const response = await fetch(allBreeds);
        const data = await response.json();
        this.setState({catList: data});
    }

    async componentDidMount() {
        await this.showCatList();
    }

    searchCat() {
        const notFoundCat = {
            image: {url: "no-image.jpg"},
            name: "",
            origin: "",
            wikipedia_url: "",
            description: "There is no such cat"
        };
        const detailCat = this.state.catList.find(item => item?.name?.toLowerCase() === this.state.searchValue.toLowerCase());
        this.setState({detailCat: detailCat ? detailCat: notFoundCat});
    }

    render() {
        return (
            <div className="wrapper">
                <div className="search-wrapper">
                    <div className="search">
                        <input
                            value={this.state.searchValue}
                            onChange={(event) => this.setState({searchValue: event.target.value}) }
                            type="text"
                            className="search_input"
                        />
                        <button className="search_btn" onClick={() => this.searchCat()}>Go</button>
                        <span className="correct-name"/>
                    </div>
                    {this.state.detailCat && (
                        <div className="cat-inf">
                            <img className="cat_img" width="400px" height="300px" alt="cat image" src={this.state.detailCat.image.url}/>
                            <p className="cat_description">{this.state.detailCat.description}</p>
                            {this.state.detailCat.name && (
                                <div className="cat_breed">
                                    <p className="breed">Breed:</p><p className="breed-name">{this.state.detailCat.name}</p>
                                </div>
                            )}
                            {this.state.detailCat.origin && (
                                <div className="cat_origin">
                                    <p className="origin">Origin:</p>
                                    <p className="origin-name">{this.state.detailCat.origin}</p>
                                </div>
                            )}
                            {this.state.detailCat.wikipedia_url && (
                                <a className="look_on_wiki" target="_blank" href={this.state.detailCat.wikipedia_url}>Go to Wikipedia</a>)
                            }
                        </div>
                    )}
                </div>
                {this.state.catList && (
                    <div className="all-cats">{this.state.catList.map((cat, index) => {
                        return (
                            <a className="cat-btn" target="_blank"
                               href={cat.wikipedia_url} key={index}>{cat.name}</a>
                        )
                    })}</div>
                )}

            </div>
        );
    }
}

export default CatAPI;
