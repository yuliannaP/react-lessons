import React, {useRef, useState, useEffect} from "react";

const UnControlledElement = () => {
    const firstNameRef = useRef(null);
    const lastNameRef = useRef(null);
    const formContainerRef = useRef(null);
    const [isFocused, setIsFocused] = useState(false);
    const [address, setAddress] = useState("Kiev");
    const clickHandler = (event) => {
        setIsFocused(formContainerRef.current?.contains(event.target));
    }
    useEffect(() => {
        document.addEventListener("click", clickHandler);
        return () => {

        }
    }, []);
    return (
        <div ref={formContainerRef} style={{border: isFocused? "1px solid red": "none"}}>
            <form onSubmit={(event) => {
                event.preventDefault();
                alert(`${firstNameRef.current.value} ${lastNameRef.current.value} ${address}`)
            }}>
                <input ref={firstNameRef} name="firstName"/>
                <input ref={lastNameRef} name="lastName"/>
                <input name="address" value={address} onChange={(event) => {
                    setAddress(event.target.value);
                }}/>
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}

export default UnControlledElement;
