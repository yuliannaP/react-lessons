import React, {useEffect, useState, useContext} from "react";
import ThemesContext from "../../Contexts/ThemesContext";


export const FactInfo = (props) => {
    const {themeValue} = useContext(ThemesContext);
    console.log("themeValue", themeValue);
    const {info} = props;
    const [infoList, setInfoList] = useState([]);
    useEffect(() => {
        const infoList = info.map((item => `${item.type} => ${item.data}`));
        setInfoList(infoList);
    }, [info]);
    return (<div>{infoList.map((item, index) => {
        return <div key={index} style={{backgroundColor: themeValue.background, color: themeValue.color}}>{item}</div>
    })}</div>)
}
