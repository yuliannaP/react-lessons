import React, {useEffect, useState, useContext} from "react";
import axios from "axios";

import {FactInfo} from "./FactInfo";
import themes from "../../themes";
import ThemesContext from "../../Contexts/ThemesContext";


const InterestingFacts = () => {
    const api = "http://numbersapi.com/1942/date";
    const getApi = (number, infoType) => `http://numbersapi.com/${number}/${infoType}`;
    const infoTypes = ["trivia", "year", "date", "math"];
    const {themeValue, setThemeValue} = useContext(ThemesContext);
    const getInfo = (number, infoType) => {
        const url = getApi(number, infoType);
        return axios.get(url);
    }
    const [info, setInfo] = useState([]);
    const [number, setNumber] = useState("1942");
    const numbers = ["1942", "1953", "1989"];
    useEffect(() => {
        const infoList = infoTypes.map((infoName) => getInfo(number, infoName));
        Promise.all(infoList).then((resultList) => {
            setInfo(resultList.map((result, index) => ({data: result.data, type: infoTypes[index]})))
        })
    }, [number]);

    return (
        <>
            <select name="theme" onChange={(e) => setThemeValue(themes[e.target.value])}>
                {Object.keys(themes).map((item) => {
                    return (<option value={item} selected={themeValue === item}>{item}</option>)
                })}
            </select>
            <select name="select" onChange={(e) => setNumber(e.target.value)}>
                {numbers.map((item) => {
                    return (<option value={item} selected={number === item}>{item}</option>)
                })}
            </select>
            <FactInfo info={info} />
        </>
        )

}

export default InterestingFacts;
