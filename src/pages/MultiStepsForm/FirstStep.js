import React, {useState, useEffect} from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import {useDispatch} from "react-redux";
import {updateFirstStep, updateCurrentStep} from "../../state/actionCreators/multiStepsFormActionCreators";
import axios from "axios";

const FirsStep = ({initialValues, onSave}) => {
    const dispatch = useDispatch();
    const validationSchema = Yup.object().shape({
        "firstName": Yup.string()
            .max(10, "First Name must be at most 10 characters")
            .required("First Name is required"),
        "lastName": Yup.string()
            .required("Last Name is required"),
    });
    const getImage = async() => {
        const img = await axios.get("https://coffee.alexflipnote.dev/random.json");
        console.log("img", img);
    }
    useEffect(() => {
        getImage();
    }, []);
    return (
        <div>
            <h1>First Step</h1>
            <Formik
                initialValues={initialValues}
                onSubmit={(values, actions) => {
                    updateFirstStep(dispatch, values);
                    updateCurrentStep(dispatch, "secondStep");
                }}
                validationSchema={validationSchema}
            >
                {props => (
                    <form onSubmit={props.handleSubmit}>
                        <div>
                            <label>
                                First name
                                <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.firstName}
                                    name="firstName"
                                />
                            </label>
                            {props.touched.firstName && props.errors.firstName && <div id="feedback">{props.errors.firstName}</div>}
                        </div>
                        <div>
                            <label>
                                Last name
                                <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.lastName}
                                    name="lastName"
                                />
                            </label>
                            {props.touched.lastName && props.errors.lastName && <div id="feedback">{props.errors.lastName}</div>}
                        </div>
                        <button type="submit">Submit</button>
                    </form>
                )}
            </Formik>
        </div>
    )};

export default FirsStep;
