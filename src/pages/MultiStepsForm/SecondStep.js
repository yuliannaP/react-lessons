import React from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import {useDispatch} from "react-redux";

import {updateSecondStep, updateLoadingState, updateFormStatus} from "../../state/actionCreators/multiStepsFormActionCreators";

const SecondStep = ({initialValues, onSave}) => {
    const dispatch = useDispatch();
    const validationSchema = Yup.object().shape({
        "email": Yup.string()
            .email("Invalid email address")
            .required("Email is required"),
        "address": Yup.string()
            .required("Address is required"),
    });
    return (
        <div>
            <h1>Second Step</h1>
            <Formik
                initialValues={initialValues}
                onSubmit={(values, actions) => {
                    updateLoadingState(dispatch, true);
                    setTimeout(() => {
                        updateSecondStep(dispatch, values);
                        updateFormStatus(dispatch, true);
                    }, 1000)


                }}
                validationSchema={validationSchema}

            >
                {props => (
                    <form onSubmit={props.handleSubmit}>
                        <div>
                            <label>
                                Email
                                <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.email}
                                    name="email"
                                />
                            </label>
                            {props.touched.email && props.errors.email && <div id="feedback">{props.errors.email}</div>}
                        </div>
                        <div>
                            <label>
                                Address
                                <input
                                    type="text"
                                    onChange={props.handleChange}
                                    onBlur={props.handleBlur}
                                    value={props.values.address}
                                    name="address"
                                />
                            </label>
                            {props.touched.age && props.errors.age && <div id="feedback">{props.errors.age}</div>}
                        </div>
                        <button type="submit">Submit</button>
                    </form>
                )}
            </Formik>
        </div>
    )};

export default SecondStep;
