import React, {useState} from "react";
import {useSelector} from "react-redux";
import FirstStep from "./FirstStep";
import SecondStep from "./SecondStep";
import { useNavigate } from 'react-router-dom';


const firstStepInitialValue = {
    firstName: "",
    lastName: ""
};

const secondStepInitialValue = {
    address: "",
    email: ""
};

const ContractForm = () => {
    const [currentStep, setCurrentStep] = useState("firstStep");
    const [value, setValue] = useState({...firstStepInitialValue, ...secondStepInitialValue});
    const formState = useSelector((state) => state.form);
    console.log("formState", formState);
    let navigate = useNavigate();
    const onSave = (stepValue, nextStep) => {
        setValue({...value, ...stepValue});
        setCurrentStep(nextStep);
    }
    if(formState.data.isSubmitted) {
        return (
            <div>
                <div>Submitted!</div>
                {/*<div>*/}
                {/*    {Object.keys(formState.data).map(key => {*/}
                {/*        return <div>{`${key} => ${formState.data[key]}`}</div>*/}
                {/*    })}*/}
                {/*</div>*/}
            </div>


        )
    }
    return (
        <div>
            <p onClick={() => navigate(`/steps/${Math.random() * (10 - 1 + 10)}`)}>Change URL</p>
            Contract Form
            {formState.data.currentStep === "firstStep" && (
                <FirstStep initialValues={formState.data.firstStep} onSave={onSave}/>
            )}
            {formState.data.currentStep === "secondStep" && (
                <SecondStep initialValues={formState.data.secondStep} onSave={onSave}/>
            )}
        </div>

    )

}

export default ContractForm;
