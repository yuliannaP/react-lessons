import React from 'react';

const FirsStep = ({onSave}) => {
    return (
        <div>
            <h1>First Step</h1>
            <button onClick={() => onSave("secondStep")}>Next Step</button>
        </div>
    )};

export default FirsStep;
