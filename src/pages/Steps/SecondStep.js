import React from 'react';

const SecondStep = ({onSave}) => {
    return (
        <div>
            <h1>Second Step</h1>
            <button onClick={() => onSave("isSubmitted")}>Next Step</button>
        </div>
    )};

export default SecondStep;
