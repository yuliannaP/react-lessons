import React, {useState} from "react";
import FirstStep from "./FirstStep";
import SecondStep from "./SecondStep";
import {useParams} from "react-router-dom";
import { useNavigate } from 'react-router-dom';



const ContractForm = () => {
    const [currentStep, setCurrentStep] = useState("firstStep");
    const {id, next} = useParams();
    let navigate = useNavigate();
    console.log("id", id);
    console.log("next", next);

    const onSave = (nextStep) => {

        setCurrentStep(nextStep);
    }

    if(currentStep === "isSubmitted") {
        return (
            <div>
                <div>Submitted!</div>

            </div>


        )
    }
    return (
        <div>
            <p onClick={() => navigate('/')}>Change url</p>
            {currentStep === "firstStep" && (
                <FirstStep onSave={onSave}/>
            )}
            {currentStep === "secondStep" && (
                <SecondStep onSave={onSave}/>
            )}
        </div>

    )

}

export default ContractForm;
