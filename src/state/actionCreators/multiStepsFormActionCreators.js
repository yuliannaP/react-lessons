import * as Actions from "../actions/multiStepsFormActions";

export const updateFirstStep = (dispatch, data) => {
    dispatch({type: "UPDATE_FIRST_STEP", payload: data});
};

export const updateSecondStep = (dispatch, data) => {
    dispatch(Actions.updateSecondStep(data));
};

export const updateCurrentStep = (dispatch, data) => {
    dispatch(Actions.updateCurrentStep(data));
};

export const updateFormStatus = (dispatch, data) => {
    dispatch(Actions.updateFormStatus(data));
};

export const updateLoadingState = (dispatch, data) => {
    dispatch(Actions.updateLoadingState(data));
};
