export const updateFirstStep = (data) => ({type: "UPDATE_FIRST_STEP", payload: data});
export const updateSecondStep = (data) => ({type: "UPDATE_SECOND_STEP", payload: data});
export const updateCurrentStep = (data) => ({type: "UPDATE_CURRENT_STEP", payload: data});
export const updateFormStatus = (data) => ({type: "UPDATE_STATUS", payload: data});
export const updateLoadingState = (data) => ({type: "UPDATE_LOADING", payload: data});
