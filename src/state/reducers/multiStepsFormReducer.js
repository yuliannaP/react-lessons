const initialState = {
    isLoading: false,
    error: null,
    data: {
        isSubmitted: false,
        currentStep: "firstStep",
        firstStep: {
            firstName: "",
            lastName: ""
        },
        secondStep: {
            address: "",
            email: ""
        }
    }
};

export const contractFormReducer = (state = initialState, action) => {
    switch (action.type) {
        case "UPDATE_FIRST_STEP": return {...state, data: {...state.data, firstStep: action.payload}};
        case "UPDATE_SECOND_STEP": return {...state, data: {...state.data, secondStep: action.payload}};
        case "UPDATE_CURRENT_STEP": return {...state, data: {...state.data, currentStep: action.payload}};
        case "UPDATE_STATUS": return {...state, data: {...state.data, isSubmitted: action.payload}};
        case "UPDATE_LOADING": return {...state, isLoading: action.payload}
        default: return state;
    }
}
