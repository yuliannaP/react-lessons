import {contractFormReducer} from "../state/reducers/multiStepsFormReducer";

export const rootReducers = {
    form: contractFormReducer,
}
