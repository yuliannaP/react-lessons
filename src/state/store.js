import {createStore, combineReducers} from "redux";

import {rootReducers} from "./rootReduser";

const store = createStore(combineReducers(rootReducers));

export default store;
